package app;

import com.example.design_patterns.creational_pattern.singleton.CacheSingleton;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/v1")
@org.springframework.stereotype.Controller
public class Controller {
    @GetMapping(value = "/singleton")
    public String singleton() {
        CacheSingleton singleton = CacheSingleton.getInstance();
        singleton.setStatus("true");
        return "true";
    }

    @GetMapping(value = "/getSingleton")
    public String getSingleton() {
        CacheSingleton singleton = CacheSingleton.getInstance();
        return singleton.getStatus();
    }
}
