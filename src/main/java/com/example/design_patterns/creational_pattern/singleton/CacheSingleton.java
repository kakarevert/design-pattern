package com.example.design_patterns.creational_pattern.singleton;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class CacheSingleton {
    private CacheSingleton(){}

    private String status;


    private static class SingletonHelper{
        private static final CacheSingleton INSTANCE = new CacheSingleton();
    }

    public static CacheSingleton getInstance(){
        return SingletonHelper.INSTANCE;
    }

    public String setStatus(String status){
        this.status = status;
        return status;
    }

    public String getStatus(){
        return this.status;
    }
}
